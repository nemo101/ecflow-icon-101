TAG = alpine-ecflow
NOTEBOOK = 202303-ecflow-ICON.ipynb
all: run

run:
	jupyter-lab ${NOTEBOOK}

slides:
	jupyter nbconvert ${NOTEBOOK} --to slides --post serve

pslides:
	jupyter nbconvert pyflow.ipynb --to slides --post serve

realclean: clean
	- rm -rf pyflow troika ecflow

clean:
	-\rm *slides.html *~
	- jupyter nbconvert --clear-output --inplace  *ipynb */*ipynb

self:
	git clone ssh://git@git.ecmwf.int/~map/ecflow-icon-training.git

pyflow:
	git clone https://github.com/ecmwf/pyflow.git
	cd pyflow && PYTHONPATH=$PYTHONPATH:/usr/local/lib/python3.8/site-packages  python3 ./setup.py build

ecflow-git:
	git clone https://github.com/ecmwf/ecflow.git

troika:
	git clone https://github.com/ecmwf/troika.git
	cd troika && python3 ./setup.py build

get: cmake boost.tgz ecflow.tgz

cmake:
	wget -O cmake https://github.com/Kitware/CMake/releases/download/v3.12.4/cmake-3.12.4.tar.gz

boost.tgz:
	wget -o boost_1_71_0.tar.gz https://boostorg.jfrog.io/artifactory/main/release/1.71.0/source/boost_1_71_0.tar.gz
	ln -sf boost_1_71_0.tar.gz boost.tgz

ecflow.tgz:
	wget -O ecFlow.zip "https://github.com/ecmwf/ecflow/archive/refs/heads/develop.zip"
	wget -O ecFlow.tgz "https://confluence.ecmwf.int/download/attachments/8650755/ecFlow-5.10.0-Source.tar.gz?api=v2"
pod:
	podman build --tag alpine-ecflow -f Dockerfile
pod-run:
	podman run alpine-ecflow ecflow_client --help
build:
	docker build -t ${TAG} .
ash:
	docker run --net=host -ti ${TAG} ash
clt:
	docker run --net=host -ti ${TAG} ecflow_client --help
svr:
	docker run --net=host -ti ${TAG} ecflow_server --port 2500

test:
	python3 -c "import ecflow; help(ecflow)"
	python3 -c "import ecflow.ecf; help(ecflow.ecf)"
	python3 -c "import pyflow; help(pyflow)"
	python3 -c "import matplotlib"

ecflow_ui:
	brew install ecflow-ui
